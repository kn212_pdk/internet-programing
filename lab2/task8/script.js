const $number = document.getElementById("num");
const $button = document.getElementById("myButton");
const $text1 = document.getElementById("text1");
const $text2 = document.getElementById("text2");
const startColor = $text1.style.color;
let start=true;
let count=10;
let answer;
$button.addEventListener("click",() => {
    if(start){
        $text1.style.color=startColor;
        $number.value="";
        $text1.textContent = "Введіть число від 1 до 1000 в поле вище і натисніть кнопку 'Перевірити'!";
        $text2.textContent = "Кількість спроб: " + count;
        $button.textContent = "Перевірити"
        answer = Math.round(Math.random()*1000);
        //console.log(answer); // для перевірки
        start=false;
    }
    else if(count>1){
        if($number.value===""){
            $text1.textContent = "Введіть число!";
            count++;
            $button.textContent = "Старт"
        }
        else if(parseFloat($number.value)>answer)$text1.textContent = "Задумане число менше ніж " + $number.value;
        else if(parseFloat($number.value)<answer)$text1.textContent = "Задумане число більше ніж " + $number.value;
        else {
            $text1.style.color='#00FF00';
            $text1.textContent = "Вітаю! Ви вгадали число!";
            $button.textContent = "Старт"
            start=true;
            count=10;
        }
        if(start)$text2.textContent = "Щоб почати заново натисніть кнопку 'Старт'";
        else {
            count--;
            $text2.textContent = "Кількість спроб: " + count;
        }
    }
    else
    {
        $text1.style.color= '#FF0000';
        $text1.textContent = "Ви програли!Задумане число: " + answer;
        start=true;
        count=10;
        $button.textContent = "Старт"
        $text2.textContent = "Щоб почати заново натисніть кнопку 'Старт'";
    }
});