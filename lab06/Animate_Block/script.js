const $widthBar=document.getElementById("widthBar");
const $heightBar=document.getElementById("heightBar");
const $rotateBar=document.getElementById("rotateBar");
const $widthBox=document.getElementById("widthBox");
const $heightBox=document.getElementById("heightBox");
const $rotateBox=document.getElementById("rotateBox");
const $block=document.getElementById("block");
$widthBar.addEventListener('change',()=>{
   $widthBox.textContent=$widthBar.value;
   $block.style.width=$widthBar.value+"px";
});
$heightBar.addEventListener('change',()=>{
   $heightBox.textContent=$heightBar.value;
   $block.style.height=$heightBar.value+"px";
});
$rotateBar.addEventListener('change',()=>{
   $rotateBox.textContent=$rotateBar.value;
   $block.style.transform=`rotate(${$rotateBar.value}deg)`;
});