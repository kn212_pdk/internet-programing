const $colors=document.getElementsByTagName("td");
const $mixColor=document.getElementById("mixColor");
const startColor=$mixColor.style.backgroundColor;
let selected=[];
for (let i=0;i<$colors.length;i++){
    $colors[i].addEventListener("click",()=>{
        if(selected.indexOf($colors[i])===-1)selected.push($colors[i]);
        else selected.splice(selected.indexOf($colors[i]),1);
        $colors[i].classList.toggle("selected");
        $mixColor.style.background='';
        if (selected.length===1){
            $mixColor.style.backgroundColor=window.getComputedStyle(selected[0]).backgroundColor;
        }
        else if (selected.length===0) {
            $mixColor.style.backgroundColor=startColor;
        }
        else {
            let gradient="linear-gradient(180deg";
            for (let i=0;i<selected.length;i++){
                gradient+=", "+window.getComputedStyle(selected[i]).backgroundColor+" "+100/(selected.length-1)*i+"%"
            }
            gradient+=")";
            $mixColor.style.background=gradient;
        }
    });
}