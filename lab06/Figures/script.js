const $figures=document.getElementsByClassName("figure");
const figArr=["square","round","triangle"];
const $box=document.getElementById('box');
const size=50;
let $active;
for(let i=0;i<$figures.length;i++){
    $figures[i].addEventListener('click',()=>{
        $active=document.getElementsByClassName('active')[0];
        if ($active!==undefined)$active.classList.remove('active');
        $figures[i].classList.add('active');
    });
}
$box.addEventListener('click', ev => {
    $active=document.getElementsByClassName('active')[0];
    if($active!==undefined) {
        let elem = document.createElement("div");
        for (let i = 0; i < figArr.length; i++) {
            if ($active.classList.contains(figArr[i])) {
                elem.classList.add(figArr[i]);
            }
        }
        $box.appendChild(elem);
        elem.style.position = "absolute";
        elem.style.top = ev.clientY - size / 2 + "px";
        elem.style.left = ev.clientX - size / 2 + "px";
    }
});