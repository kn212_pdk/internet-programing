let $year = document.getElementById("year");
let $month = document.getElementById("month");
const $button = document.getElementById("myButton");
$button.addEventListener("click", () => {
 if($year.value >= 0)
 {
     let intercalary;
     if(($year.value % 4 === 0 && $year.value % 100 !== 0) || ($year.value / 100 % 4 === 0))intercalary=true;
     else intercalary=false;
     if(intercalary)console.log("Рік є високосним");
     else console.log("Рік не є високосним");
     if($month.value>0 && $month.value < 13)
     {
         let monthName,season;
         switch (parseInt($month.value)){
             case 1: monthName = "Січень";
                 season="Зима";
                 break;
             case 2: monthName = "Лютий";
                 season="Зима";
                 break;
             case 3: monthName = "Березень";
                 season="Весна";
                 break;
             case 4: monthName = "Квітень";
                 season="Весна";
                 break;
             case 5: monthName = "Травень";
                 season="Весна";
                 break;
             case 6: monthName = "Червень";
                 season="Літо";
                 break;
             case 7: monthName = "Липень";
                 season="Літо";
                 break;
             case 8: monthName = "Серпень";
                 season="Літо";
                 break;
             case 9: monthName = "Вересень";
                 season="Осінь";
                 break;
             case 10: monthName = "Жовтень";
                 season="Осінь";
                 break;
             case 11: monthName = "Листопад";
                 season="Осінь";
                 break;
             case 12: monthName = "Грудень";
                 season="Зима";
                 break;
         }
         console.log("Пора року: "+ season);
         console.log("Місяць:" + monthName);
         if($month.value==1 || $month.value==3 || $month.value==5 || $month.value==7 || $month.value==8 || $month.value==10 || $month.value==12) console.log("Кількість днів у місяці: 31");
         else if($month.value==4 || $month.value==6 || $month.value==9 || $month.value==11) console.log("Кількість днів у місяці: 30");
         else if($month.value==2 && intercalary)console.log("Кількість днів у місяці: 29");
         else console.log("Кількість днів у місяці: 28");
     }
     else console.log("Місяць введено неправильно!");
 }
 else console.log("Рік введено неправильно!");
});