const product1 = {
    name: 'Keyboard',
    price: 100,
    quantity:128,
    getTotalPrice(){
        return this.quantity*this.price;
    }
}
const product2 = {
    name: 'Mouse',
    price: 30,
    quantity:193,
    getTotalPrice(){
        return this.quantity*this.price;
    }
}
console.log(`The price of ${product1.name} is ${product1.price} euro`);
console.log(`The price of ${product2.name} is ${product2.price} euro`);
console.log(`The total price of ${product1.name} is ${product1.getTotalPrice()} euro`);
console.log(`The total price of ${product2.name} is ${product2.getTotalPrice()} euro`);