function User(name,age){
    this.name=name;
    this.age=age;
    this.getAgeCategory=()=>{
        if(this.age<10)return "Child";
        if(this.age<16)return "Teenager";
        if(this.age<20)return "Young";
        if(this.age<40)return "Adult";
        if(this.age<60)return "Middle";
        if(this.age<80)return "Old";
        return "Centenarian"
    }
}
let inputUsers=[
    {name:"Jane",age:6},
    {name:"John",age:14},
    {name:"Michael",age:18},
    {name:"Alex",age:35},
    {name:"Mykola",age:55},
    {name:"Anna",age:78},
    {name:"Maria",age:96},
];
let users = [];
inputUsers.forEach(function (user){
    users.push(new User(user.name,user.age));
});
users.forEach(user=>{
    console.log(`${user.name},${user.age},${user.getAgeCategory()}`);
})
