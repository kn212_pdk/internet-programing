let user={
    firstName:"Ксенія",
    lastName:"Бичак",
    languages:[
        {title:"C#",description:"Об'єктно-орієнтована мова"},
        {title:"HTML-CSS",description:"Мови розмітки і стилю сторінки"},
        {title:"JS",description:"Динамічна,об'єктно-орієнтована прототипна мова програмування"}
    ],
    addLanguage(title, description){
        let fl=false;
        for (let i=0; i<this.languages.length;i++) {
            if (this.languages[i].title === title) fl = true;
        }
        if(!fl)this.languages.push({title:title,description:description});
        else document.getElementById('info').textContent='Ця мова вже є в списку!';
        this.getAllLanguages();
    },
    getLanguage(title){
        for (let i=0; i<this.languages.length;i++){
            if(this.languages[i].title===title){
                document.getElementById('info').textContent=this.languages[i].title + ' - ' + this.languages[i].description;
                break;
            }
            if(i+1===this.languages.length)document.getElementById('info').textContent='Таку мову не знайдено!';
        }
    },
    getAllLanguages()
    {
        let $langcontainer = document.getElementById('langs');
        if(this.languages.length!==0) {
            let list = Array(this.languages.length);
            for (let i = 0; i < this.languages.length; i++) {
                list[i] = this.languages[i].title
            }
            $langcontainer.textContent = list.join(', ');
        }
        else $langcontainer.textContent='Мов програмування не знайдено!';
    },
    removeLanguage(title){
        for (let i=0; i<this.languages.length;i++){
            if(this.languages[i].title===title){
                this.languages.splice(i,1);
                break;
            }
            if(i+1===this.languages.length)document.getElementById('info').textContent='Таку мову не знайдено!';
        }
        this.getAllLanguages();
    }
}
const $add = document.getElementById('add');
const $remove = document.getElementById('remove');
const $infoButton =document.getElementById('infobutton');
const $title = document.getElementById('title');
const $description = document.getElementById('description');
document.getElementById('user').textContent+=user.lastName + user.firstName;
user.getAllLanguages();
$add.addEventListener('click',() => {
    if($description.value!=='') {
        document.getElementById('info').textContent='';
        user.addLanguage($title.value, $description.value);
    }
    else document.getElementById('info').textContent='Введіть опис мови програмування!';
});
$remove.addEventListener('click',() => user.removeLanguage($title.value));
$infoButton.addEventListener('click',() => user.getLanguage($title.value));
