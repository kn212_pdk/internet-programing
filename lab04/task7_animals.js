const animals=[
    {class: Cat, args: {name: "Bars", weight: 15}},
    {class: Dog, args: {name: "Spot", weight: 25}},
    {class: Cat, args: {name: "Murzyk", weight: 12}},
    {class: Cat, args: {name: "Whiskers", weight: 13}},
    {class: Dog, args: {name: "Fluffy", weight: 30}},
    {class: Dog, args: {name: "Fido", weight: 12}}
];
function Cat(name,weight){
    this.name=name;
    this.weight=weight;
    this.say =()=>{
        if(this.weight<13)return "Miu";
        return "Meow";
    }
}
function Dog(name,weight){
    this.name=name;
    this.weight=weight;
    this.say =()=>{
        if(this.weight<25)return "Yip";
        return "Woof";
    }
}
let catdogs=[];
animals.forEach(animal=>{
    catdogs.push(new animal.class(animal.args.name,animal.args.weight));
});
catdogs.forEach(pet=>{
    console.log(pet.say());
});

