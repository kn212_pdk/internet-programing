//Констрктор Car
function Car({make,model,fuel,started}){
    this.make=make;
    this.model=model;
    this.fuel=fuel;
    this.started=started;
    this.start = ()=>{
        if(this.started)return `${this.make} ${this.model} уже заведений`;
        if(this.fuel>0){
            this.started=true;
            return `${this.make} ${this.model} завівся`;
        }
        return `У ${this.make} ${this.model} немає бензину`;
    };
    this.stop = ()=>{
        if (!this.started)return `${this.make} ${this.model} уже заглушений`;
        this.started=false;
        return `${this.make} ${this.model} було заглушено`;
    };
    this.drive = distance=>{
        if (!this.started)return "Потрібно завести автомобіль"
        if (distance==='')return `${this.make} ${this.model} не рухається (потрібно ввести дистанцію)`
        if(this.fuel>0){
            if (this.fuel<distance)distance=this.fuel;
            this.fuel-=distance;
            let text = `${this.make} ${this.model} проїхав ${distance} літрів бензину. `;
            if(this.fuel===0){
                this.started=false;
                text+="Бензин закінчився і автомобіль заглох";
            }
            return text;
        }

    };
    this.addFuel = amount=>{
        if (amount==='')return 'Потрібно ввести кількість бензину';
        this.fuel+=parseInt(amount);
        return `Взято ${this.fuel} літрів бензину для ${this.make} ${this.model}, заправлено автомобіль.`;
    };
}
//масив початкових даних для машин
const cars = [
    {make:"Fiat", model: "500", fuel: 0, started: false},
    {make:"GM", model: "Cadillac", fuel: 50, started: false},
    {make:"Webwille Motors", model: "Taxi", fuel: 10, started: true}
];
//машини
const fiat = new Car(cars[0]);
const cadi = new Car(cars[1]);
const taxi = new Car(cars[2]);
let selectCar=fiat;
//ініціалізація елементів сторінки
const $listCars=document.getElementById('cars');
const $imageCar=document.getElementById('imageCar');
const $textFuel=document.getElementById('fuel');
const $textState=document.getElementById('state');
const $startButton=document.getElementById('start');
const $stopButton=document.getElementById('stop');
const $driveButton=document.getElementById('drive');
const $distance=document.getElementById('distance');
const $addButton=document.getElementById('addButton');
const $addFuel=document.getElementById('addFuel');
const $textInfo=document.getElementById('info');
//Зміна машини в select
$listCars.addEventListener('change',()=>{
    if($listCars.value == 0){
        selectCar=fiat;
        $imageCar.src='fiat.jpg';
    }
    else if($listCars.value == 1){
        selectCar=cadi;
        $imageCar.src='cadilac.jpg';
    }
    else{
        selectCar=taxi;
        $imageCar.src='taxi.jpg';
    }
    $imageCar.alt=selectCar.make +' '+selectCar.model;
    $textFuel.textContent=selectCar.fuel;
    if(selectCar.started)$textState.textContent='Заведений';
    else $textState.textContent='Незаведений';
    $textInfo.textContent='';
});
//Заведення машини
$startButton.addEventListener('click',()=>{
    $textInfo.textContent=selectCar.start();
    if(selectCar.fuel>0)$textState.textContent='Заведений';
});
//Зупинка машини
$stopButton.addEventListener('click',()=>{
    $textInfo.textContent=selectCar.stop()
    $textState.textContent='Незаведений';
});
//Рух машини
$driveButton.addEventListener('click',()=>{
    $textInfo.textContent=selectCar.drive($distance.value);
    if (selectCar.started) $textState.textContent='Заведений';
    else $textState.textContent='Незаведений';
    $textFuel.textContent=selectCar.fuel;
});
//Заправка машини
$addButton.addEventListener('click',()=>{
    $textInfo.textContent=selectCar.addFuel($addFuel.value);
    $textFuel.textContent=selectCar.fuel;
});