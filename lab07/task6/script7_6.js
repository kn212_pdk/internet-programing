//classes
class Shape {
    color;
    rotate;
    width;
    height;
    round;

    constructor(color, rotate, width, height) {
        this.color = color;
        this.rotate = rotate;
        this.width = width;
        this.height = height;
    };

    figureGeneration($elem) {
        $elem.style.backgroundColor = this.color;
        $elem.style.transform = `rotate(${this.rotate}deg)`
        $elem.style.width = this.width;
        $elem.style.height = this.height;
        if (this.round) $elem.style.borderRadius = "50% / 50%";
        else $elem.style.borderRadius = null;
    }
}

//program
const $brushes = document.getElementById("brush").children;
let shapeBrush = new Shape("black", 0, "70px", "70px");
const $figure = document.getElementById("figure");
const $colorInput = document.getElementById("colorInput");
const $widthInput = document.getElementById("widthInput");
const $heightInput = document.getElementById("heightInput");
const $rotateInput = document.getElementById("rotateInput");
const $inkLevel = document.getElementById("inkLevel");
const $box = document.getElementById("box");
const $panel = document.getElementById("panel");
let fl = false;
for (let i = 0; i < $brushes.length; i++) {
    $brushes[i].addEventListener("click", () => {
        for (let j = 0; j < $brushes.length; j++) {
            $brushes[j].classList.remove("selected");
            $figure.classList.remove($brushes[j].classList[0]);
        }
        $brushes[i].classList.add("selected");
        $figure.classList.add($brushes[i].classList[0]);
        shapeBrush.round = $brushes[i].classList[0] === "round";
        $inkLevel.value = 100;
        shapeBrush.figureGeneration($figure);
    });
}
$colorInput.addEventListener("change", () => {
    shapeBrush.color = $colorInput.value;
    shapeBrush.figureGeneration($figure);
});
$widthInput.addEventListener("change", () => {
    shapeBrush.width = $widthInput.value + "px";
    shapeBrush.figureGeneration($figure);
});
$heightInput.addEventListener("change", () => {
    shapeBrush.height = $heightInput.value + "px";
    shapeBrush.figureGeneration($figure);
});
$rotateInput.addEventListener("change", () => {
    shapeBrush.rotate = $rotateInput.value;
    shapeBrush.figureGeneration($figure);
});
$box.addEventListener("mousedown", () => {
    fl = true;
});
$box.addEventListener("mousemove", ev => {
    if (fl && $figure.classList.length !== 0) {
        console.log("mousemove");
        if ($inkLevel.value !== 0) {
            let size;
            if ($figure.style.width.slice(0, -2) < $figure.style.height.slice(0, -2)) size = $figure.style.width.slice(0, -2);
            else size = $figure.style.height.slice(0, -2);
            let top = ev.clientY - size / 2;
            let left = ev.clientX - size / 2;
            if (top > $box.clientTop && left - $panel.clientWidth > $box.clientLeft && top + Number(size) < $box.clientTop + $box.clientHeight && left + Number(size) - $panel.clientWidth < $box.clientLeft + $box.clientWidth) {
                $inkLevel.value -= 0.2;
                let $elem = document.createElement("div");
                shapeBrush.figureGeneration($elem);
                $elem.style.position = "absolute";
                $elem.style.top = top + "px";
                $elem.style.left = left + "px";
                $elem.style.opacity = $inkLevel.value + "%";
                $box.appendChild($elem);
            } else {
                fl = false;
            }
        }
    }
});
$box.addEventListener("mouseup", () => {
    fl = false;
});
