const  $pressureResult = document.getElementById("pressureResult");
const $humidityResult= document.getElementById("humidityResult");
const $temperatureResult=document.getElementById("temperatureResult");
const $imageWeather=document.getElementById("imageWeater");
const $cityWeather=document.getElementById("cityWeather");

const fetchWeather = async(url) => {
  let response = await  fetch(url);
  let  weather = await response.json()
    console.log(weather)
  $pressureResult.innerHTML=weather.main.pressure;
  $humidityResult.innerHTML=weather.main.humidity;
  $temperatureResult.innerHTML=weather.main.temp;
  $imageWeather.src=`http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`;
  $cityWeather.innerHTML=weather.name;
  document.getElementsByClassName("active")[0].classList.remove("active");
  if(document.getElementsByClassName(`${weather.name}`)[0]) {
    document.getElementsByClassName(`${weather.name}`)[0].classList.add("active");
  }
}
let city='Zhitomyr';
const  params = location.search.slice(1).split("&")
params.forEach((param)=>{
  let  array=param.split("=");
  if(array[0]=="city"){
    city=array[1];

  }
})

let apiKey='b5018676b6c9e7d01aa7056fd2b9186d';
let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`
fetchWeather(url)
