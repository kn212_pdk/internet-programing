//елементи сторінки і інші змінні
const $percent=document.getElementById('percent');
const $right=document.getElementById('right');
const $multiply=document.getElementById('multiply');
const $variants=document.getElementsByTagName('input');
const $labels=document.getElementsByTagName('label');
const $answer=document.getElementById('answer');
const $myButton=document.getElementById('myButton');
let firstmul;
let secondmul;
//функція генерації
function generation(){
    for (let i=0;i<$variants.length;i++){
        $variants[i].checked=false;
    }
    $answer.textContent='';
    firstmul=Math.round(Math.random()*(9-2)+2);
    secondmul=Math.round(Math.random()*(9-2)+2);
    $multiply.textContent=`${task})${firstmul}x${secondmul}=`;
    let indexright=Math.round(Math.random()*3);
    let rightAnswer=firstmul*secondmul;
    for (let i=0;i<4;i++){
        if (i===indexright)$labels[i].textContent=rightAnswer;
        else $labels[i].textContent=Math.round(Math.random()*(90-3)+3);
        let fl;
        do {
            fl=false;
            for (let j = 0; j < i; j++) {
                if($labels[i].textContent===$labels[j].textContent)fl=true;
            }
            if(fl)$labels[i].textContent=Math.round(Math.random()*(90-3)+3);
        }while (fl)
    }
}
//програма
let task=1;
generation();
$myButton.addEventListener('click',()=>{
    if(task<10)task++;
    else {
        task=1;
        $myButton.textContent='Наступне завдання';
        $percent.textContent=0;
        $right.textContent=0;
    }
    generation();
    if(task===10)$myButton.textContent='Перезапустити';
});
for (let i=0;i<$variants.length;i++){
    $variants[i].addEventListener('change',()=>{
        if($labels[i].textContent==firstmul*secondmul){
            $percent.textContent=parseInt($percent.textContent)+10;
            $right.textContent=parseInt($right.textContent)+1;
            $answer.textContent='Правильно!';
        }
        else {
            $answer.textContent=`Правильна відповідь "${firstmul*secondmul}"`;
        }
    });
}