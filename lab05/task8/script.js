const $puzzles = document.getElementsByClassName('puzzle');
const $container = document.getElementById('container');
const bTRBL = [10,910,710,10];//y,x,y,x - відстань до верхньої, правої, нижньої, лівої границі
for (let i = 0; i < $puzzles.length; i++) {
    $puzzles[i].style.left = Math.random()*((bTRBL[1]-125)-bTRBL[3])+bTRBL[3] + "px";
    $puzzles[i].style.top = Math.random()*((bTRBL[2]-125)-bTRBL[0])+bTRBL[0] + "px";
}
for (let i = 0; i < $puzzles.length; i++) {
    let startX;
    let startY;
    $puzzles[i].addEventListener('dragstart',ev=>{
        startX=ev.clientX;
        startY=ev.clientY;
    });
    $puzzles[i].addEventListener('dragend',ev=>{
        let position=[parseInt($puzzles[i].style.left.slice(0,-2)),parseInt($puzzles[i].style.top.slice(0,-2))];
        let finishX = position[0]+ev.clientX-startX;
        let finishY = position[1]+ev.clientY-startY;
        $puzzles[i].style.top = finishY+"px";
        if(finishX+125<bTRBL[1] || finishX>bTRBL[3])$puzzles[i].style.left = finishX+"px";
        if(finishY+125<bTRBL[2]||finishY>bTRBL[0])$puzzles[i].style.top = finishY+"px";
    });
}