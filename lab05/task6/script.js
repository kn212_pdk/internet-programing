const $myButton=document.getElementsByTagName('button')[0];
const $n=document.getElementById('rows');
const $m=document.getElementById('columns');
const $contaner=document.getElementById('container');
let $places;
$myButton.addEventListener('click',()=>{
    $contaner.innerHTML='';
    for (let i=0;i<$n.value;i++){
        $contaner.innerHTML+='<tr class="selected"></tr>';
        let $selected=document.getElementsByClassName('selected')[0];
        for (let j=0;j<$m.value;j++){
            $selected.innerHTML+='<td></td>';
        }
        $selected.classList.remove('selected')
    }
    $places=document.getElementsByTagName('td');
    for (let i=0;i<$places.length;i++){
        $places[i].addEventListener('click',()=>{
            $places[i].classList.toggle('red');
        });
    }
});

